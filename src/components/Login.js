
export default function Login(){

    return(
        <>
            <div className="login-page-container">
                <div className="login-page__accounts-login is-flex">
                    <div className="login-page__accounts-container">
                            <div className="login-page__accounts">
                                    <img src="images/fb-logo.png"/> 
                            </div>
                            <div className="login-page__accounts-texts">
                                    <p className="p_28"> Recent logins</p>
                                    <p className="p_12">Click your profile or add an account.</p>
                            </div>

                            <div className="login-page__accounts-profiles">
                                    <div className="login-page__accounts-profile1">
                                        <div className="login-page__accounts-profile1-img">
                                            <img src="images/default-profile-pic.png"></img>
                                        </div>
                                        <div className="login-page__accounts-profile1-name">
                                            Patrick
                                        </div>
                                        <div className="login-page__accounts-profile1-close">
                                        </div>
                                        <div className="login-page__accounts-profile1-notif"> 2
                                        </div>
                                    </div>
                                    <div className="login-page__accounts-profile1">
                                        <div className="login-page__accounts-add-profile">
                                            <div className="login-page__accounts-plus-button">
                                            </div>
                                        </div>
                                        <div className="login-page__accounts-add-profile-p">
                                            Add Account
                                        </div>
                                    </div>
                            </div>
                    </div>

                    <div className="login-page__form-container">
                            <div className="login-page__form">
                                <form>
                                    <input className="login-page__form-input" type="text" placeholder="Email address or phone number"/>
                                    <input className="login-page__form-input" type="text" placeholder="Password"/>
                                    <button className="login-page__submit" type="submit"> Log in</button>
                                </form>
                                <div className="login-page__forgot-pass">
                                    Forgotten Password?
                                </div>
                                <div className="login-page__horizontal-rule"></div>
                                <div className="is-flex is-justify-content-center">
                                    <button className="login-page__create" type="submit"> Create New Account</button>
                                </div>
                            </div>
                            <div className="login-page__footnote">
                                <span id="font-weight-700">Create a Page </span>for a celebrity, brand or business.
                            </div>
                    </div>
                </div>
            </div>
            <div>

            </div>
       </>
    )

}